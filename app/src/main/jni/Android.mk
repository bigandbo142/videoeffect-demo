LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
#opencv
OPENCVROOT := /Volumes/Data/workspace/OpenCV-android-sdk
OPENCV_CAMERA_MODULES := on
OPENCV_INSTALL_MODULES := on
OPENCV_LIB_TYPE := SHARED
include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk

LOCAL_SRC_FILES := main.cpp Texture.cpp Shader.cpp VideoRenderer.cpp AROverlayRenderer.cpp
LOCAL_LDLIBS += -llog -landroid -lEGL -lGLESv3 -lz -ljnigraphics
LOCAL_MODULE := CameraOpenCVLib
LOCAL_CFLAGS    := -Werror
LOCAL_C_INCLUDES += /usr/local/include/glm Texture.h Shader.hpp VideoRenderer.hpp AROverlayRenderer.hpp
LOCAL_SHARED_LIBRARIES += libavformat libavcodec libswscale libavutil libwsresample

include $(BUILD_SHARED_LIBRARY)

$(call import-module,ffmpeg/android/arm)