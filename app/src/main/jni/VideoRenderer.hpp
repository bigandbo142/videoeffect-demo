//
// Created by Đoàn Lê Ngọc Linh on 3/15/16.
//

#ifndef REALTIMEVIDEO_VIDEORENDERER_HPP
#define REALTIMEVIDEO_VIDEORENDERER_HPP


//handles the shader program and basic opengl calls
#include <Shader.hpp>
//for texture supports
#include <Texture.h>

//opencv supports
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

class VideoRenderer {
public:
    VideoRenderer();
    virtual ~VideoRenderer();
    //setup all shader program and texture mapping variables
    bool setup();
    //render the frame on the screen
    void render(cv::Mat frame);
    bool initTexture(cv::Mat frame);

private:
    //this handles the generic camera feed view
    GLuint gProgram;
    GLuint gvPositionHandle;
    GLuint vertexUVHandle;
    GLuint textureSamplerID;
    GLuint texture_id;
    Shader shader;
    Texture texture;
};



#endif //REALTIMEVIDEO_VIDEORENDERER_HPP
